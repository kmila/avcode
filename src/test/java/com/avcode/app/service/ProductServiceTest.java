package com.avcode.app.service;

import com.avcode.app.model.Product;
import com.avcode.app.model.ProductBuilder;
import com.avcode.app.repository.ProductRepository;
import com.example.mockito.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

// TODO add negative scenarios
@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private ProductService iut = new ProductService();

    private Product productFixture;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        productFixture = ProductBuilder.withId("TEST", null);
    }

    @Test
    void saveOrUpdate() {
        when(productRepository.save(productFixture)).thenReturn(productFixture);

        Product result = iut.saveOrUpdate(productFixture);

        assertEquals(productFixture, result);
    }

    @Test
    void delete() {
        when(productRepository.findOne(productFixture.getId())).thenReturn(productFixture);
        doNothing().when(productRepository).delete(productFixture);

        String deletedProductId = iut.delete(productFixture.getId());

        assertEquals(productFixture.getId(), deletedProductId);

        verify(productRepository).delete(productFixture);
    }

    @Test
    void findAll() {
        when(productRepository.findAllWithImages()).thenReturn(Collections.singleton(productFixture));

        Product result = iut.findAll(null).iterator().next();

        assertEquals(productFixture, result);
    }

    @Test
    void find() {
        when(productRepository.findByIdWithImages(productFixture.getId())).thenReturn(productFixture);

        Product result = iut.find(productFixture.getId(), null);

        assertEquals(productFixture, result);
    }

    @Test
    void findChildrenByDescription() {
        when(productRepository.findChildrenByDescription(eq(productFixture.getId()), eq(productFixture.getId()), any(Pageable.class))).thenReturn(new PageImpl<>(Collections.singletonList(productFixture)));

        Product result = iut.findChildrenByDescription(productFixture.getId(), productFixture.getId()).iterator().next();

        assertEquals(productFixture, result);
    }
}