package com.avcode.app.service;

import com.avcode.app.model.Image;
import com.avcode.app.model.ImageBuilder;
import com.avcode.app.model.Product;
import com.avcode.app.model.ProductBuilder;
import com.avcode.app.repository.ImageRepository;
import com.example.mockito.MockitoExtension;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

// TODO WIP
@ExtendWith(MockitoExtension.class)
class ImageServiceTest {

    @Mock
    private ImageRepository imageRepository;

    @InjectMocks
    private ImageService iut = new ImageService();

    private Product productFixture;

    private Image imageFixture;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        productFixture = ProductBuilder.withId("TEST", null);
        imageFixture = ImageBuilder.withProductId(productFixture.getId(), null);
    }

    @Test
    void findById() {
        iut.findById(imageFixture.getId());
    }

    @Test
    void saveOrUpdate() {
        iut.saveOrUpdate(imageFixture);
    }

    @Test
    void delete() {
        iut.delete(imageFixture.getId());
    }

    @Test
    void findAll() {
        iut.findAll(productFixture.getId());
    }

    @Test
    void findTop10() {
        iut.findTop10(productFixture.getId());
    }
}