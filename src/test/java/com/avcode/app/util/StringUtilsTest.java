package com.avcode.app.util;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.UUID;

import static com.avcode.app.util.StringUtils.MESSAGE;
import static org.junit.jupiter.api.Assertions.assertEquals;

class StringUtilsTest {

    @Test
    void jsonMessage() {
        String actual = "test";
        assertEquals(String.format("%s=%s", MESSAGE, actual), StringUtils.jsonMessage(actual).toString());
    }

    @Test
    void uuid64() {
        UUID.fromString(decodeUUID(StringUtils.uuid64()));
    }

    private static String decodeUUID(String actual) {
        return new String(Base64.getUrlDecoder().decode(actual), StandardCharsets.UTF_8);
    }

}