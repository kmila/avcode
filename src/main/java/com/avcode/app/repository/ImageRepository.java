package com.avcode.app.repository;

import com.avcode.app.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ImageRepository extends JpaRepository<Image, Long> {

    List<Image> findByProductId(String productId);

    List<Image> findTop10ByProductId(String productId);

}
