package com.avcode.app.repository;

import com.avcode.app.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    @Query(value = "SELECT p FROM Product p LEFT JOIN FETCH p.images")
    Set<Product> findAllWithImages();

    @Query(value = "SELECT DISTINCT(p) FROM Product p LEFT JOIN FETCH p.images where p.id = :id")
    Product findByIdWithImages(@Param("id") String id);

    @Query(value = "SELECT p FROM Product p where p.id = :id")
    Product findByIdWithChildren(@Param("id") String id);

    @Query(value = "SELECT new Product(p.id, p.description) FROM Product p where p.parent.id = :parentId")
    List<Product> findChildren(@Param("parentId") String parentId);

    @Query("SELECT p from Product p where p.parent.id = :parentId and p.description like %:description%")
    Page<Product> findChildrenByDescription(@Param("parentId") String parentId, @Param("description") String description, Pageable pageable);
}
