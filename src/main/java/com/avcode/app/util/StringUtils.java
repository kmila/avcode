package com.avcode.app.util;

import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Base64;
import java.util.Map.Entry;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;

public class StringUtils {

    public static final String MESSAGE = "message";

    public static Entry<?,?> jsonMessage(String msg) {
        return new SimpleImmutableEntry<>(MESSAGE, msg);
    }

    public static String uuid64() {
        String uuid = UUID.randomUUID().toString();
        return Base64.getUrlEncoder().withoutPadding().encodeToString(uuid.getBytes(UTF_8));
    }

}
