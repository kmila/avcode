package com.avcode.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import static com.avcode.app.util.StringUtils.jsonMessage;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;

@Component
@Provider
public class JaxrsExceptionMapper implements ExceptionMapper<Throwable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(JaxrsExceptionMapper.class);

    @Override
    public Response toResponse(Throwable ex) {
        Response.Status st = (ex instanceof NotFoundException) ? NOT_FOUND : BAD_REQUEST;
        if (st != NOT_FOUND) {
            LOGGER.error("Invalid request", ex);
        }
        Response.ResponseBuilder responseBuilder = Response.status(st).entity(jsonMessage(st.getReasonPhrase()));
        responseBuilder.type(MediaType.APPLICATION_JSON);
        return responseBuilder.build();
    }

}
