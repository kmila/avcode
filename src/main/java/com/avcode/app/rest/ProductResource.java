package com.avcode.app.rest;

import com.avcode.app.model.Fields;
import com.avcode.app.model.Product;
import com.avcode.app.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Set;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Path("/product")
@Component
public class ProductResource {

    @Autowired
    private ProductService productService;

    @GET
    @Produces(APPLICATION_JSON)
    public Set<Product> products(@QueryParam("fields") String f) {
        Fields fields = Fields.create(f);
        Set<Product> product = productService.findAll(fields);
        return product;
    }

    @POST
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Response create(Product p) {
        p = productService.saveOrUpdate(p);
        return Response.created(URI.create("/product/" + p.getId())).entity(p).build();
    }

}
