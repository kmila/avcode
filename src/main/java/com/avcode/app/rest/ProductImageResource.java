package com.avcode.app.rest;

import com.avcode.app.model.Fields;
import com.avcode.app.model.Image;
import com.avcode.app.model.Product;
import com.avcode.app.service.ImageService;
import com.avcode.app.service.ProductService;
import com.avcode.app.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collections;
import java.util.List;

import static com.avcode.app.model.ImageBuilder.withId;
import static com.avcode.app.model.ImageBuilder.withProductId;
import static com.avcode.app.model.ImageBuilder.withType;
import static com.avcode.app.model.ProductBuilder.withId;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

/**
 * JAX-RS subresource helper
 */
@Path("/product/{product_id}")
@Component
public class ProductImageResource {

    @Autowired
    private ProductService productService;

    @Autowired
    private ImageService imageService;

    @GET
    @Produces(APPLICATION_JSON)
    public Product findById(@PathParam("product_id") String productId, @QueryParam("fields") String fields) {
        Product product = productService.find(productId, Fields.create(fields));
        return product;
    }

    @PUT
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Product update(@PathParam("product_id") String productId, Product p) {
        Product updatedProduct = withId(productId, p);
        return productService.saveOrUpdate(updatedProduct);
    }

    @DELETE
    @Produces(APPLICATION_JSON)
    public Response delete(@PathParam("product_id") String productId) {
        String id = productService.delete(productId);
        return (id == null ? Response.notModified() : Response.noContent()).build();
    }

    /* products */

    @GET
    @Produces(APPLICATION_JSON)
    public List<Product> productSet(@PathParam("product_id") String parentId, @QueryParam("fields") String fields, @QueryParam("description") String description) {
        if (description == null || description.isEmpty()) {
            return Collections.singletonList(findById(parentId, fields));
        }
        List<Product> result = productService.findChildrenByDescription(parentId, description);
        return result;
    }

    /* images */

    @GET
    @Path("/image")
    @Produces(APPLICATION_JSON)
    public List<Image> images(@PathParam("product_id") String productId) {
        List<Image> images = imageService.findAll(productId);
        return images;
    }

    @GET
    @Path("/images")
    @Produces(APPLICATION_JSON)
    public List<Image> imageSet(@PathParam("product_id") String productId) {
        List<Image> images = imageService.findTop10(productId);
        return images;
    }

    @POST
    @Path("/image")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Response create(@PathParam("product_id") String productId, Image i) {
        i = imageService.saveOrUpdate(withProductId(productId, withId(null, i)));
        return Response.created(URI.create("/product/" + productId + "/image" + i.getId())).entity(i).build();
    }

    @GET
    @Path("/image/{id}")
    @Produces(APPLICATION_JSON)
    public Image findById(@PathParam("product_id") String productId, @PathParam("id") Long imageId) {
        Image image = imageService.findById(imageId);
        return image.getProduct().getId().equals(productId) ? image : null;
    }

    @PUT
    @Path("/image/{id}")
    @Produces(APPLICATION_JSON)
    @Consumes(APPLICATION_JSON)
    public Image update(@PathParam("product_id") String productId, @PathParam("id") Long imageId, Image i) {
        Image image = findById(productId, imageId);
        i = withType(i.getType(), image);
        return imageService.saveOrUpdate(i);
    }

    @DELETE
    @Path("/image/{id}")
    @Produces(APPLICATION_JSON)
    public Response delete(@PathParam("id") Long imageId) {
        Long id = imageService.delete(imageId);
        return (id == null ? Response.notModified() : Response.noContent()).build();
    }

}
