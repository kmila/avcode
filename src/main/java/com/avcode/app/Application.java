package com.avcode.app;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import javax.annotation.PostConstruct;

/**
 * SpringBoot entry point application
 */
@Configuration
@SpringBootApplication
public class Application extends SpringBootServletInitializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @PostConstruct
    public void postConstruct() {
        LOGGER.warn("Boot configuration overridden");
    }

    @Bean
    public Jackson2ObjectMapperBuilder objectMapperBuilder() {
        // TODO reset objectmapper on RESTEasy as follows to use the same ObjectMapper as spring-mvc
        Jackson2ObjectMapperBuilder builder = Jackson2ObjectMapperBuilder.json();
        objectMapperBuilderCustomizer().customize(builder);
        return builder;
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer objectMapperBuilderCustomizer() {
        return builder -> {
            builder.defaultUseWrapper(true);
            builder.autoDetectFields(true);
            builder.autoDetectGettersSetters(false);
            builder.defaultViewInclusion(false);
            builder.failOnUnknownProperties(false);
            builder.indentOutput(false);
        };
    }

}
