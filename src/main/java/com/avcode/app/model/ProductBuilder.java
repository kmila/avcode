package com.avcode.app.model;

import java.util.Collection;
import java.util.List;

// FIXME side-effects by changing the reference attributes as a hack to the lack of JsonView, return a copy instead
public class ProductBuilder {

    public static Product withDescription(String desc, Product p) {
        if (p == null) {
            p = new Product();
        }
        p.description = desc;
        if (p.parentProductId != null) {
            p.parent = withId(p.parentProductId, p.parent);
        }
        return p;
    }

    public static Product withId(String id, Product p) {
        if (p == null) {
            p = new Product();
        }
        p.id = id;
        return p;
    }

    public static Product withImages(Product p, List<Image> images) {
        return withChildren(p, images, null);
    }

    public static Product withChildren(Product p, List<Image> images, Collection<Product> children) {
        if (p == null) {
            p = new Product();
        }
        p.children = children;
        p.images = images;
        return p;
    }

}
