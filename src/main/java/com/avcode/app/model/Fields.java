package com.avcode.app.model;

public enum Fields {

    NONE, IMAGES, CHILDREN;

    public static Fields create(String fields) {
        try {
            return (fields == null) ? null : Fields.valueOf(fields.toUpperCase());
        } catch (Exception e) {
            return IMAGES;
        }
    }

}
