package com.avcode.app.model;

// TODO replace side-effects by a copy of the model
public class ImageBuilder {

    public static Image withType(String type, Image i) {
        if (i == null) {
            i = new Image();
        }
        i.type = type;
        return i;
    }

    public static Image withId(Long id, Image i) {
        if (i == null) {
            i = new Image();
        }
        i.id = id;
        return i;
    }

    public static Image withProductId(String product_id, Image i) {
        if (i == null) {
            i = new Image();
        }
        i.product = ProductBuilder.withId(product_id, null);
        return i;
    }

}
