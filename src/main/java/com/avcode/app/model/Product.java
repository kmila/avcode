package com.avcode.app.model;

import com.avcode.app.util.StringUtils;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.PUBLIC_ONLY;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@Entity
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = PUBLIC_ONLY, isGetterVisibility = NONE, setterVisibility = NONE)
@JsonInclude(NON_NULL)
public class Product {

    @Id
    String id = StringUtils.uuid64();

    String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_PRODUCT_ID", foreignKey = @ForeignKey(name = "FK_PARENT"))
    @JsonIgnore
    Product parent;

    @OneToMany(mappedBy = "parent")
    Collection<Product> children;

    @OneToMany(mappedBy = "product", orphanRemoval = true)
    @JsonManagedReference
    List<Image> images = new ArrayList<Image>();

    transient String parentProductId;

    Product() {
        // jpa
    }

    public Product(String id, String description) {
        this.id = id;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public Collection<Product> getChildren() {
        if (children instanceof Set) {
            return Collections.unmodifiableSet((Set) children);
        }
        if (children instanceof List) {
            return Collections.unmodifiableList((List) children);
        }
        return null;
    }

    public List<Image> getImages() {
        return (images == null) ? null : Collections.unmodifiableList(images);
    }

    public String getDescription() {
        return description;
    }

    public Product getParent() {
        return parent;
    }

    public String getParentProductId() {
        return parentProductId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(id, product.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return id;
    }

}