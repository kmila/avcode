package com.avcode.app.service;

import com.avcode.app.model.Fields;
import com.avcode.app.model.Product;
import com.avcode.app.repository.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.avcode.app.model.ProductBuilder.withChildren;
import static com.avcode.app.model.ProductBuilder.withDescription;
import static com.avcode.app.model.ProductBuilder.withImages;

@Service
public class ProductService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProductService.class);

    @Autowired
    private ProductRepository productRepository;

    @Transactional
    public Product saveOrUpdate(Product p) {
        Product product = productRepository.save(withDescription(p.getDescription(), p));
        LOGGER.debug("saveOrUpdate {} -> {}", p, product);
        return product;
    }

    @Transactional
    public String delete(String id) {
        Product product = productRepository.findOne(id);
        if (product == null) {
            return null;
        }
        LOGGER.warn("Deleting {}", id);
        productRepository.delete(product);
        return id;
    }

    @Transactional(readOnly = true)
    public Set<Product> findAll(Fields fields) {
        Set<Product> list = productRepository.findAllWithImages();
        // workaround to fetch relationships eagerly (TODO replace with single query)
        initLazy(fields, list);
        return list;
    }

    private <T extends Collection<Product>> T initLazy(Fields fields, T list) {
        for (Product product : list) {
            withChildren(product, (fields == Fields.NONE) ? null : product.getImages(), (fields == Fields.NONE) ? null : findChildren(product.getId()));
        }
        return list;
    }

    @Transactional(readOnly = true)
    public Product find(String productId, Fields fields) {
        if (productId == null) {
            return null;
        }
        Product product = productRepository.findByIdWithImages(productId);
        if (product == null) {
            return null;
        }
        if (fields == Fields.CHILDREN) {
            // workaround to fetch relationships eagerly (TODO replace with single query)
            return withChildren(productRepository.findByIdWithChildren(productId), product.getImages(), findChildren(productId));
        }
        return withImages(product, (fields == Fields.NONE) ? null : product.getImages());
    }

    @Transactional(readOnly = true)
    public List<Product> findChildrenByDescription(String parentId, String description) {
        Page<Product> products = productRepository.findChildrenByDescription(parentId, description, new PageRequest(0, 100));
        return products == null ? null : initLazy(Fields.NONE, products.getContent());
    }

    private List<Product> findChildren(String parentProductId) {
        return productRepository.findChildren(parentProductId);
    }

}
