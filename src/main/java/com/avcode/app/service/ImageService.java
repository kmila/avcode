package com.avcode.app.service;

import com.avcode.app.model.Image;
import com.avcode.app.repository.ImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ImageService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageService.class);

    @Autowired
    private ImageRepository imageRepository;

    @Transactional
    public Image findById(Long id) {
        return imageRepository.findOne(id);
    }

    @Transactional
    public Image saveOrUpdate(Image i) {
        // TODO handle transient product
        Image image = imageRepository.save(i);
        return image;
    }

    @Transactional
    public Long delete(Long id) {
        LOGGER.warn("Deleting {}", id);
        Image image = imageRepository.findOne(id);
        if (image == null) {
            return null;
        }
        imageRepository.delete(image);
        return id;
    }

    @Transactional(readOnly = true)
    public List<Image> findAll(String productId) {
        List<Image> image = imageRepository.findByProductId(productId);
        return image;
    }

    @Transactional(readOnly = true)
    public List<Image> findTop10(String productId) {
        return imageRepository.findTop10ByProductId(productId);
    }

}
