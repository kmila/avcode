package com.avcode.app;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * JAX-RS application
 */
@Component
@ApplicationPath("/api/")
public class JaxrsApplication extends javax.ws.rs.core.Application {

    // TODO configure JAX-RS objectMapper
    @Component
    @Provider
    public static class JaxrsJsonProvider implements ContextResolver<ObjectMapper> {

        @Autowired
        // FIXME ignored by RESTEasy
        private ObjectMapper mapper;

        @Override
        public ObjectMapper getContext(Class<?> type) {
            return mapper;
        }

    }

}
